<?php

function genModal($colName)
{

    require("databaseConnect.php");

    $sql = "SELECT * FROM rowList WHERE columnName = '$colName'";

    $result = $conn->query($sql);

    $correctColName = str_replace(' ', '-', $colName);
    echo $correctColName;

    $sql = "SELECT * FROM Rates";

    $result2 = $conn->query($sql);

    if ($result2->num_rows > 0) {

        // output data of each row
        while ($row = $result2->fetch_assoc()) {

            $internR = $row["internRate"];
            $juniorBookR = $row["juniorRateBook"];
            $juniorConsultR = $row["juniorRateConsult"];
            $seniorBookR = $row["seniorRateBook"];
            $seniorConsultR = $row["seniorRateConsult"];
            $masterR = $row["masterRate"];
        }
    }

    $count = 0;
    if ($result->num_rows > 0) {

        // output data of each row

        while ($row = $result->fetch_assoc()) {
            $count++;
            $rowName = $row["rowName"];
            $rowCode = $row["rowCode"];
            $once = $row["payOnce"];
            $retain = $row["payRetain"];
            $type = $row["inputType"];

            echo "<div class=\"modal\" id=\"$rowCode\" role=\"dialog\">

            <div class=\"modal-dialog\">
                <!-- Modal content-->
                <div class=\"modal-content\">
                    <div class=\"modal-header\">
                        <button type=\"button\" class=\"close\"
                                data-dismiss=\"modal\">&times;
                        </button>
                        <h4 class=\"modal-title\">$rowName
                        </h4>
                    </div>
                    <div class=\"modal-body\">
                    <div class=\"input-group input-group-sm\">
                    <form>
                            ";
            if ($once == "1" && !$retain == "1") {
                echo "<input checked type=\"radio\" id=\"$rowCode--Once\" name=\"payment\"
                                       value=\"once\">
                                Once off costs
                                <br>";
            }
            if ($retain == "1" && !$once == "1") {
                echo "<input checked type=\"radio\" id=\"$rowCode--Monthly\" name=\"payment\"
                                       value=\"monthly\"
                                       >
                                Included in retainer";
            }

            if ($retain == "1" && $once == "1") {
                echo "<input checked type=\"radio\" id=\"$rowCode--Once\" name=\"payment\"
                                       value=\"once\">
                                Once off costs
                                <br>";

                echo "<input type=\"radio\" id=\"$rowCode--Monthly\" name=\"payment\"
                                       value=\"monthly\"
                                       >
                                Included in retainer";
            }

            echo "</form>
                            <hr>";

            $internY = $row["internExp"];
            $juniorBookY = $row["juniorExpBook"];
            $juniorConsultY = $row["juniorExpConsult"];
            $seniorBookY = $row["seniorExpBook"];
            $seniorConsultY = $row["seniorExpConsult"];
            $masterY = $row["masterExp"];

            $internH = $row["interDefHours"];
            $juniorBookH = $row["juniorBookDefHours"];
            $juniorConsultH = $row["juniorConsultDefHours"];
            $seniorBookH = $row["seniorBookDefHours"];
            $seniorConsultH = $row["seniorConsultDefHours"];
            $masterH = $row["masterDefHours"];

            if ($type == "Hours") {

                echo "     <form>
                                
                                ";

                if ($masterY == "1" || $seniorConsultY == "1" || $seniorBookY == "1" || $juniorConsultY == "1" || $juniorBookY == "1" || $internY == "1") {
                    echo "<h4>Select experience level:</h4>";
                }

                if ($masterY == "1") {
                    echo "<input type=\"radio\" id=\"$rowCode--master\" name=\"level\"
                                       value=\"master\"
                                       onclick=\"document.getElementById('$rowCode--Hours').value = $masterH\"
                                >
                                Master<br>";
                }

                if ($seniorConsultY == "1") {
                    echo "<input type=\"radio\" id=\"$rowCode--seniorConsult\" name=\"level\"
                                       value=\"seniorConsult\"
                                       onclick=\"document.getElementById('$rowCode--Hours').value = $seniorConsultH\"
                                >
                                Senior consultant<br>";
                }

                if ($seniorBookY == "1") {
                    echo "<input type=\"radio\" id=\"$rowCode--seniorBook\" name=\"level\"
                                       value=\"seniorBook\"
                                       onclick=\"document.getElementById('$rowCode--Hours').value = $seniorBookH\"
                                >
                                Senior bookkeeper<br>";
                }

                if ($juniorConsultY == "1") {
                    echo "<input type=\"radio\" id=\"$rowCode--juniorConsult\" name=\"level\"
                                       value=\"juniorConsult\"
                                       onclick=\"document.getElementById('$rowCode--Hours').value = $juniorConsultH\"
                                >
                                Junior consultant<br>";
                }

                if ($juniorBookY == "1") {
                    echo "<input type=\"radio\" id=\"$rowCode--juniorBook\" name=\"level\"
                                       value=\"juniorBook\"
                                       onclick=\"document.getElementById('$rowCode--Hours').value = $juniorBookH\"
                                >
                                Junior bookkeeper<br>";
                }

                if ($internY == "1") {
                    echo "<input type=\"radio\" id=\"$rowCode--intern\" name=\"level\"
                                       value=\"intern\"
                                       onclick=\"document.getElementById('$rowCode--Hours').value = $internH\"
                                >
                                Intern<br>";
                }
                $fullColName = "" . $correctColName . "-Result";

                if ($masterY == "1" || $seniorConsultY == "1" || $seniorBookY == "1" || $juniorConsultY == "1" || $juniorBookY == "1" || $internY == "1") {
                    echo "<hr>";
                }

                echo "
                                <h4>Input amount of hours:</h4>
                                <div class=\"input-group\">
                                                            <span class=\"input-group-addon\"
                                                            >Hours</span>
                                    <input type=\"number\"
                                           class=\"form-control numHours\" id=\"$rowCode--Hours\"
                                           aria-describedby=\"basic-addon3\">
                                </div>
                                <hr>
                                <button type=\"button\" class=\"btn btn-default\"
                                        onclick=\"HoursFunc('$rowCode',$('#$rowCode--Hours').val(),$('#$rowCode--Monthly:checked').val(),$('#$rowCode--intern:checked').val(),$('#$rowCode--juniorBook:checked').val(),$('#$rowCode--juniorConsult:checked').val(),$('#$rowCode--seniorBook:checked').val(),$('#$rowCode--seniorConsult:checked').val(),$('#$rowCode--master:checked').val(),'$fullColName', '$rowName','$correctColName')\"
                                        data-dismiss='modal'                
                                        >
                                    Submit
                                </button>
</form>
                            ";

            }
            if ($type == "KM") {

                $kmPrice = $row["kmRate"];
                $fullColName = "" . $correctColName . "-Result";
                echo "    <form>
      <div class=\"input-group\">
                                                            <span class=\"input-group-addon\"
                                                            >KM</span>
                                <input type=\"number\"
                                       class=\"form-control numHours\" id=\"$rowCode--KM\"
                                       aria-describedby=\"basic-addon3\">
                            </div>
                            <hr>
                            <button type=\"button\" class=\"btn btn-default\"
                                    onclick=\"kmFunc('$rowCode',$('#$rowCode--KM').val(),$('#$rowCode--Monthly:checked').val(),'$kmPrice','$fullColName','$rowName','$correctColName')\"
                                    data-dismiss=\"modal\">
                                Submit
                            </button>
                            </form>
                        ";

            }
            if ($type == "Fixed") {
                $fixed = $row["fixedRate"];
                $fullColName = "" . $correctColName . "-Result";
                echo "<form>
                                <h4>Fixed Price : </h4>
                                <p>R <span id=\"$rowCode.Price\">$fixed</span></p>
                                <hr>
                                Quantity:
                                <div class=\"input-group\"><span class=\"input-group-addon\">QTY</span> <input type=\"number\"
                                                                                                           class=\"form-control numHours\"
                                                                                                           id=\"$rowCode--Qty\"
                                                                                                           aria-describedby=\"basic-addon3\">
                                </div>
                                <hr>
                                <button type=\"button\" class=\"btn btn-default\"
                                        onclick=\"fixedFunc('$rowCode','$fixed',$('#$rowCode--Qty').val(),$('#$rowCode--Monthly:checked').val(),'$fullColName','$rowName','$correctColName')\"
                                        data-dismiss=\"modal\">
                                    Submit
                                </button>
                                </form>";
            }
            if ($type == "Dynfixed") {



                echo "     <form>
                                
                                ";

                if ($masterY == "1" || $seniorConsultY == "1" || $seniorBookY == "1" || $juniorConsultY == "1" || $juniorBookY == "1" || $internY == "1") {
                    echo "<h4>Select experience level:</h4>";
                }

                if ($masterY == "1") {
                    echo "<input type=\"radio\" id=\"$rowCode--master\" name=\"level\"
                                       value=\"master\"
                                       onclick=\"document.getElementById('$rowCode--Price').innerHTML = $masterR\"
                                >
                                Master<br>";
                }

                if ($seniorConsultY == "1") {
                    echo "<input type=\"radio\" id=\"$rowCode--seniorConsult\" name=\"level\"
                                       value=\"seniorConsult\"
                                       onclick=\"document.getElementById('$rowCode--Price').innerHTML = $seniorConsultR\"
                                >
                                Senior consultant<br>";
                }

                if ($seniorBookY == "1") {
                    echo "<input type=\"radio\" id=\"$rowCode--seniorBook\" name=\"level\"
                                       value=\"seniorBook\"
                                       onclick=\"document.getElementById('$rowCode--Price').innerHTML = $seniorBookR\"
                                >
                                Senior bookkeeper<br>";
                }

                if ($juniorConsultY == "1") {
                    echo "<input type=\"radio\" id=\"$rowCode--juniorConsult\" name=\"level\"
                                       value=\"juniorConsult\"
                                       onclick=\"document.getElementById('$rowCode--Price').innerHTML = $juniorConsultR\"
                                >
                                Junior consultant<br>";
                }

                if ($juniorBookY == "1") {
                    echo "<input type=\"radio\" id=\"$rowCode--juniorBook\" name=\"level\"
                                       value=\"juniorBook\"
                                       onclick=\"document.getElementById('$rowCode--Price').innerHTML = $juniorBookR\"
                                >
                                Junior bookkeeper<br>";
                }

                if ($internY == "1") {
                    echo "<input type=\"radio\" id=\"$rowCode--intern\" name=\"level\"
                                       value=\"intern\"
                                       onclick=\"document.getElementById('$rowCode--Price').innerHTML = $internR\"
                                >
                                Intern<br>";
                }
                $fullColName = "" . $correctColName . "-Result";

                if ($masterY == "1" || $seniorConsultY == "1" || $seniorBookY == "1" || $juniorConsultY == "1" || $juniorBookY == "1" || $internY == "1") {
                    echo "<hr>";
                }

                echo "<form>
                                <p>R<span id=\"$rowCode--Price\">0</span></p>
                                <hr>
                                Quantity:
                                <div class=\"input-group\"><span class=\"input-group-addon\">QTY</span> <input type=\"number\"
                                                                                                           class=\"form-control numHours\"
                                                                                                           id=\"$rowCode--Qty\"
                                                                                                           aria-describedby=\"basic-addon3\">
                                </div>
                                <hr>
                                <button type=\"button\" class=\"btn btn-default\"
                                        onclick=\"DynFixedFunc('$rowCode',$('#$rowCode--Qty').val(),$('#$rowCode--Monthly:checked').val(),$('#$rowCode--intern:checked').val(),$('#$rowCode--juniorBook:checked').val(),$('#$rowCode--juniorConsult:checked').val(),$('#$rowCode--seniorBook:checked').val(),$('#$rowCode--seniorConsult:checked').val(),$('#$rowCode--master:checked').val(),'$fullColName','$rowName','$correctColName')\"
                                        data-dismiss=\"modal\">
                                    Submit
                                </button>
                                </form>";

            }
            if ($type == "Pages") {
                $pageRate = $row["pageRate"];
                $fullColName = "" . $correctColName . "-Result";
                echo "<form>
                                <h4>Page price : </h4>
                                <p>R <span id=\"$rowCode.Price\">$pageRate</span></p>
                                <hr>
                                Quantity:
                                <div class=\"input-group\"><span class=\"input-group-addon\">QTY of pages</span> <input type=\"number\"
                                                                                                           class=\"form-control numHours\"
                                                                                                           id=\"$rowCode--Qty\"
                                                                                                           aria-describedby=\"basic-addon3\">
                                </div>
                                <hr>
                                <button type=\"button\" class=\"btn btn-default\"
                                        onclick=\"pageFunc('$rowCode','$pageRate',$('#$rowCode--Qty').val(),$('#$rowCode--Monthly:checked').val(),'$fullColName','$rowName','$correctColName')\"
                                        data-dismiss=\"modal\">
                                    Submit
                                </button>
                                </form>";
            }
            echo "
                        </div>
                    </div>
                    <div class=\"modal-footer\">
                        <button type=\"button\" class=\"btn btn-default\"
                                data-dismiss=\"modal\">
                            Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
";
        }

    }
    echo $count;
    $conn->close();
}

