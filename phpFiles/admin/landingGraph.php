<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 2017/02/07
 * Time: 12:49 PM
 */

function generateGraph(){

    //This will generate the graph for the Admin to track the amount of invoices generated

    echo "<div id='piechart' class='float' style='height: 750px; background-color: transparent'></div>
                <script type=\"text/javascript\">
                    google.charts.load('current', {'packages': ['corechart']});
                    google.charts.setOnLoadCallback(drawChart);
                    function drawChart() {

                        var data = google.visualization.arrayToDataTable([
                            ['Task', 'Invoices handed out'],
                            ['Monday', 11],
                            ['Tuesday', 5],
                            ['Wednesday', 10],
                            ['Thursday', 1],
                            ['Friday', 20],
                            ['Saturday', 30],
                            ['Sunday', 40]
                        ]);

                        var options = {
                            title: 'Number of invoices handed out in the last week'
                        };

                        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

                        chart.draw(data, options);
                    }
                </script>";
}